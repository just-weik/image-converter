<?php
	if(!isset($_POST)){
		header("Location:convert-gambar-index.php");
	}

	$jlhgambar = $_POST['jlhgambar'];
	if($jlhgambar > 0){
		$error = false;

		/* untuk yg pakai base64 */
		// foreach($_POST['gambar'] as $val){
		// 	$base64 = explode(',', $val['ctx'])[1];
		// 	$filename = $val['nama'];
		// 	$gambar = base64_decode($base64);
		// 	$success = file_put_contents('gambar-compress/' . $filename, $gambar);
		// 	if(!$success){
		// 		$error = true;
		// 		break;
		// 	}
		// }

		/* untuk yg pakai formdata */
		// print_r($_FILES);
		for($x = 0; $x < $jlhgambar; $x++){
			$tmpname = $_FILES['img'.$x]['tmp_name'];
			$filename = pathinfo($_FILES['img'.$x]['name'], PATHINFO_FILENAME) . ".webp";
			$error = !move_uploaded_file($tmpname, 'gambar-compress/' . $filename);
			if($error) break;
		}

		if($error) $data = array( 'status' => 'error', 'msg' => 'Ada gambar yang gagal di-convert');
		else $data = array( 'status' => 'success', 'msg' => 'Semua gambar telah dikonversi');
	}else{
		$data = array( 'status' => 'error', 'msg' => 'Tidak ada gambar yang dikirim');
	}

	echo json_encode($data);
?>