<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Image Converter and Compressor</title>
		<style>
			:root {
				--height: 350px;
			}
			* { box-sizing : border-box; }
			.form { margin-bottom: 15px; }
			.hasil {
				width: calc(var(--height) * 3);
				margin : 0 auto;
				display: flex;
				flex-wrap: wrap;
				flex-direction: row;
			}
			.hasil .col {
				width: var(--height);
				height: var(--height);
				border: 1px solid #000000;
				display: flex;
				justify-content: center;
				align-items: center;
				position: relative;
			}
			.hasil .col span:not(.nama-file) {
				position: absolute;
				top: 5px;
				left : 8px;
				font-weight: bold;
				font-size: 16px;
			}
			.hasil .col span.nama-file {
				position: absolute;
				bottom: 0px;
				width: 350px;
				text-align: center;
			}
			.hasil .col canvas {
				background-color: #000000;
			}
			.hide {
				display: none;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<input type="file" multiple id="gambar"/>
			<button id="proses">Convert</button>
			<button id="reset">Reset</button>
			<span class="upload hide">Uploading...</span>
		</div>
		<div class="hasil"></div>
	</body>
	<script src="../jquery.min.js"></script>
	<script>
		var params = new FormData();
		const imgAllowed = {
			type : ['image/jpg', 'image/jpeg', 'image/png', 'image/webp'],
			maxSize : 2097152,
			quality : 0.8,
			convertType : 'image/webp',
		};

		$(document).ready(() => {
			$('.hasil').html(``);
		});

		function muatGambar(loadedImg){
			var y = 1;
			loadedImg.forEach((item) => {
				item.onload = function(){
					var ct = document.getElementById(`canvas-${ y }`); // id canvas
					document.getElementById(`nama-${y}`).innerHTML = this.fileName;
					var xpos, ypos, wimg, himg;
					ct.width = 300;
					ct.height = 300;
					if(this.width >= this.height){
						wimg = ct.width;
						himg = ct.width / this.width * this.height;
						xpos = 0;
						ypos = (ct.height - himg) / 2;
					}else{
						himg = ct.height;
						wimg = ct.height / this.height * this.width;
						xpos = (ct.width - wimg) / 2;
						ypos = 0;
					}
					var ctx = ct.getContext('2d', { preserveDrawingBuffer: false });
					ctx.drawImage(this, xpos, ypos, wimg, himg);
					y++;
				};
			});
		}

		$('#gambar').change(() => {
			var loadedImg = [], x = 1;
			$('.hasil').html('');
      // console.log($('#gambar')[0].files.length);
			for(x = 1; x <= $('#gambar')[0].files.length; x++){
				var e = $('#gambar')[0].files[x-1];
				$('.hasil').append(`<div class="col">
															<span>${ x }</span>
															<canvas id="canvas-${ x }"></canvas>
															<span class="nama-file" id="nama-${ x }"></span>
														</div>`);
				loadedImg[x-1] = new Image();
				loadedImg[x-1].src = URL.createObjectURL(e);
				loadedImg[x-1].fileName = e.name;
			}
			muatGambar(loadedImg);
		});

		function reset(){
			$('#gambar').val(null);
			$('.hasil').html(``);
			$('#proses, #reset, #gambar').attr('disabled', false);
			$('.upload').addClass("hide");
		}

		$('#reset').click(() => reset());

		function loading(){
			$('.upload').removeClass('hide');
			$('#proses, #reset, #gambar').attr('disabled', true);
		}

		$('#proses').click(function(){
			loading();
			var gambar = [];
			var jlhgambar = $('#gambar')[0].files.length;

			var params = new FormData();
			params.append('jlhgambar', jlhgambar);

			var promises = $.map($('.hasil').find('canvas'), function(item) {
				return new Promise(function (resolve) {
					var ctx = document.getElementById(item.id);
					ctx.toBlob(resolve, imgAllowed.convertType, imgAllowed.quality);
				});
			});

			Promise.all(promises).then(blobs => {
				var x = 0;
				blobs.forEach(item => {
					var namaFile = $('.hasil').find(`#nama-${ x+1 }`).text();
					params.append(`img${x}`, item, namaFile);
					x++;
				});

				$.ajax({
					type : 'POST',
					method : 'POST',
					url : 'convert-gambar-proses.php',
					data : params, // $_FILES
					contentType : false, // base64 need to comment this
					processData : false, // base64 need to comment this
					cache : false, // base64 need to comment this
					success : function(resp){
						alert(resp.msg);
						if(resp.status == 'success') reset();
					},
					dataType : 'json'
				});
			});
		});
	</script>
</html>